import filter from "./array-filter.0.js";
import { expect } from "chai";

describe("array-filter", function () {
  it("returns an empty array for an empty query", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({ array: data, query: {} });
    expect(filtered_data).eql([]);
  });
});
