import filter from "./array-filter.3.js";
import { expect } from "chai";

describe("array-filter", function () {
  it("returns an empty array for an empty query", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({ array: data, query: {} });
    expect(filtered_data).eql([]);
  });
  it("filters the data by a sample single-column data item", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: "a",
      },
    });
    expect(filtered_data).eql([
      {
        id: "a",
      },
    ]);
  });
  it("filters the data by a different single-column data item", function () {
    const data = [{ id: "a" }, { id: "b", extra: "c" }];
    let filtered_data = filter({
      array: data,
      query: {
        extra: "c",
      },
    });
    expect(filtered_data).eql([
      {
        id: "b",
        extra: "c",
      },
    ]);
  });
  it("filters the data by several column data items", function () {
    const data = [
      {
        id: 1,
        first: "John",
        last: "Smith",
      },
      {
        id: 2,
        first: "John",
        last: "Doe",
      },
    ];
    let filtered_data = filter({
      array: data,
      query: {
        first: "John",
        last: "Doe",
      },
    });
    expect(filtered_data).eql([
      {
        id: 2,
        first: "John",
        last: "Doe",
      },
    ]);
  });
});
