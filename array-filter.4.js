export default function filter({ array, query = {} }) {
  if (Object.keys(query).length === 0) {
    return [];
  }
  return array.filter(function (row) {
    let include_row = true;
    for (let [field, rule] of Object.entries(query)) {
      if (typeof rule !== "object") {
        rule = { $eq: rule };
      }
      include_row = include_row && row[field] === rule.$eq;
    }
    return include_row;
  });
}
