import filter from "./array-filter.6.js";
import { expect } from "chai";

describe("array-filter", function () {
  it("returns an empty array for an empty query", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({ array: data, query: {} });
    expect(filtered_data).eql([]);
  });
  it("filters the data by a sample single-column data item", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: "a",
      },
    });
    expect(filtered_data).eql([
      {
        id: "a",
      },
    ]);
  });
  it("filters the data by a different single-column data item", function () {
    const data = [{ id: "a" }, { id: "b", extra: "c" }];
    let filtered_data = filter({
      array: data,
      query: {
        extra: "c",
      },
    });
    expect(filtered_data).eql([
      {
        id: "b",
        extra: "c",
      },
    ]);
  });
  it("filters the data by several column data items", function () {
    const data = [
      {
        id: 1,
        first: "John",
        last: "Smith",
      },
      {
        id: 2,
        first: "John",
        last: "Doe",
      },
    ];
    let filtered_data = filter({
      array: data,
      query: {
        first: "John",
        last: "Doe",
      },
    });
    expect(filtered_data).eql([
      {
        id: 2,
        first: "John",
        last: "Doe",
      },
    ]);
  });
  it("accepts the $eq operator", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: {
          $eq: "a",
        },
      },
    });
    expect(filtered_data).eql([
      {
        id: "a",
      },
    ]);
  });
  it("accepts the $lt operator for strings", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: { $lt: "b" },
      },
    });
    expect(filtered_data).eql([{ id: "a" }]);
  });
  it("accepts the $lte operator for strings", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: { $lte: "b" },
      },
    });
    expect(filtered_data).eql([{ id: "a" }, { id: "b" }]);
  });
  it("accepts the $gt operator for strings", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: { $gt: "a" },
      },
    });
    expect(filtered_data).eql([{ id: "b" }]);
  });
  it("accepts the $gte operator for strings", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: { $gte: "a" },
      },
    });
    expect(filtered_data).eql([{ id: "a" }, { id: "b" }]);
  });
  it("accepts the $neq operator for strings", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: { $neq: "a" },
      },
    });
    expect(filtered_data).eql([{ id: "b" }]);
  });
});
