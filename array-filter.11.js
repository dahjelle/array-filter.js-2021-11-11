const comparison_operators = {
  $eq: function (a, b) {
    return a === b;
  },
  $lt: function (a, b) {
    return a < b;
  },
  $neq: function (a, b) {
    return a !== b;
  },
  $lte: function (a, b) {
    return a <= b;
  },
  $gt: function (a, b) {
    return a > b;
  },
  $gte: function (a, b) {
    return a >= b;
  },
};

const combination_operators = {
  $and: function ({ row, query }) {
    return query.$and.reduce(function (accumulator, subquery) {
      return accumulator && doesRowFitQuery({ row, query: subquery });
    }, true);
  },
  $or: function ({ row, query }) {
    return query.$or.reduce(function (accumulator, subquery) {
      return accumulator || doesRowFitQuery({ row, query: subquery });
    }, false);
  },
};

function doesRowFitQuery({ row, query }) {
  const query_keys = Object.keys(query);
  const operator = query_keys[0];
  const is_combination_query =
    query_keys.length === 1 && combination_operators[operator] !== undefined;

  // if we have a combination operator, calculate the appropriate result
  if (is_combination_query) {
    return combination_operators[operator]({ row, query });
  }

  let include_row = true;
  for (let [field, rule] of Object.entries(query)) {
    if (typeof rule !== "object") {
      rule = { $eq: rule };
    }
    for (const [operator, value] of Object.entries(rule)) {
      include_row =
        include_row && comparison_operators[operator](row[field], value);
    }
  }
  return include_row;
}

export default function filter({ array, query = {} }) {
  if (Object.keys(query).length === 0) {
    return [];
  }
  return array.filter(function (row) {
    return doesRowFitQuery({ row, query });
  });
}
