import filter from "./array-filter.1.js";
import { expect } from "chai";

describe("array-filter", function () {
  it("returns an empty array for an empty query", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({ array: data, query: {} });
    expect(filtered_data).eql([]);
  });
  it("filters the data by a sample single-column data item", function () {
    const data = [{ id: "a" }, { id: "b" }];
    let filtered_data = filter({
      array: data,
      query: {
        id: "a",
      },
    });
    expect(filtered_data).eql([
      {
        id: "a",
      },
    ]);
  });
});
