export default function filter({ array, query = {} }) {
  if (Object.keys(query).length === 0) {
    return [];
  }
  const field = Object.keys(query)[0];
  const value = Object.values(query)[0];
  return array.filter(function (row) {
    return row[field] === value;
  });
}
