export default function filter({ array, query = {} }) {
  if (Object.keys(query).length === 0) {
    return [];
  }
  return array.filter(function (row) {
    let include_row = true;
    for (const [field, value] of Object.entries(query)) {
      include_row = include_row && row[field] === value;
    }
    return include_row;
  });
}
