const data = [{ id: 1 }, { id: 2 }, { id: 3 }];
const result = filter({
  array: data,
  query: {
    $or: [
      {
        id: 1,
      },
      {
        $and: [
          {
            id: {
              $gt: 1,
            },
          },
          {
            id: {
              $lt: 3,
            },
          },
        ],
      },
    ],
  },
});
console.log(result); // [{ id: 1 }, { id: 2 }]
