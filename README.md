# `array-filter.js`: A MongoDB-like query language on JavaScript arrays

>>>
- talk notes: https://gitlab.com/dahjelle/array-filter.js-2021-11-11/-/blob/main/README.md
- repo: https://gitlab.com/dahjelle/array-filter.js-2021-11-11/
- slides files listed in the [`slide-index.txt`](https://gitlab.com/dahjelle/array-filter.js-2021-11-11/-/blob/main/slide-index.txt) file
>>>

## `slide1.txt`: Introduction of Me

My name is David Hjelle. I work at Icon Systems, Inc. in Moorhead. We're a small company (9 employees) that does church software — keeping track of people, donations, and accounting. I've been there since 2009.

## `slide.2.js`: Introduction of Talk

So, this is a pretty code-heavy talk, but hopefully you'll be able to follow along at least a little bit of JavaScript isn't your day-to-day language. Put on your nerd hat!

Here's what we are going to build: a function `filter` that takes an array of JavaScript objects, and can satisfy a few MongoDB-like queries, like so:

```javascript
const data = [{ id: 1 }, { id: 2 }, { id: 3 }];
const result = filter({array: data, query: {
  $or: [{
    id: 1
  }, {
    $and: [{
      id: {
        $gt: 1
      },
    }, {
      id: {
        $lt: 3
      }
    }]
  }]
}});
console.log(result); // [{ id: 1 }, { id: 2 }]
```

When I first started implementing something like this, I figured it would be a bit involved. Possible, but involved. It turns out that you can do a version of this in 65 lines of JavaScript! Now, that doesn't have indexes or any fancy optimizations — but it works, and performance is probably fine on simple queries for up to thousands or tens of thousands of records.

## `slide.3.js` Super-fast introduction to the query language.

But, before we get too far, I should introduce the query language a bit better. This is not at all a complete re-implementation of MongoDB's query language, but it is a useful subset that operates well on at least one common JavaScript data structure.

I assume that the data is basically tabular: rows of columns, also known as an array of JavaScript objects with key-value pairs. Something like:

```javascript
[{
  id: 1,
  first: "John",
  last: "Smith"
}, {
  id: 2,
  first: "John",
  last: "Doe"
}]
```

## `slide.4.js`

Queries are also JSON. (This means we don't have to write a parser! Hooray!) A very simple query is simply the name of the field as the key and the desired value of the field as a value. This is what it looks like for exact matches.

```javascript
{
  last: "Smith"
}
```

Running that query will return a single row of data with John Smith's data.

## `slide.5.js`

That query is actually just syntactic sugar for a more verbose equality query:

```javascript
{
  last: {
    $eq: "Smith"
  }
}
```

Here, the key is exactly the same, but the value is also a JavaScript object whose key-value pair represents the _operator_ we want to use to query the data — in this, case, equality.

There is more to the query language we are going to implement — and **lots** more to the MongoDB version — but I think this is enough to get us started. I'll introduce the other bits as they come. (And, frankly, if you are a Mongo expert, you can probably correct quite a few assumptions I make along the way…)

## `array-filter.test.0.js`

Here's a first test. (I'm using `mocha` and `chai` for the tests, for no particular reason other than that's what the production code is using.) The main thing is that we are:

1. setting up the array we are going to filter on
2. call the `filter` function with the data and an empty query
3. make sure that the `filtered_data` we get is an empty array

## `array-filter.0.js`

To pass, we'll do the simplest implementation possible — `return [];`.

And tests pass! Hooray!

## `array-filter.test.1.js`

Let's add another test, adding in a query. This query looks for an exact match on the `id` field.

## `array-filter.1.js`

First, we need to preserve the existing behavior. Let's add a conditional for that.

Otherwise, we want to `filter` the array for any rows that match the criteria. Let's assume that there is a single criteria, and that it is looking for an exact match on the `id` field. So, we get the `value` that the query is looking for, and then we just return all rows where `row.id === value`.

## `array-filter.test.2.js`

Let's not hard-code the `id` label anymore. First, we'll add in something to the demo data to key off of — `extra: "c"` — and create a query to match only that.

## `array-filter.2.js`

For this to work, we need to not only get the `value` out of the field, but also the `field` label. We're still assuming that queries are only one key-value pair, so we can just get the first key-value pair in the query. Then, we replace the `.id` with `[field]`, and we're good to go.

## `array-filter.test.3.js`

Let's start combining criteria. For instance, say we want to match **both** first **and** last name at the same time.

## `array-filter.3.js`

How would you change the code we have to handle an arbitrary number of key-value pairs in the query?

Let's loop over the key-value pairs in the query, taking the `field` and the `value` from each. Then, we combine the criteria with `&&` for each data element.

## `array-filter.test.4.js`

So far, we've been using the shortcut syntax for equality (or `$eq`) queries: listing just the field name and the value. Next, we want to support the full syntax:

```json
{
  id: {
    $eq: "a"
  }
}
```

Same test as before, just with the expanded syntax.

## `array-filter.4.js`

To implement this, we check and see if the `rule` is an object. If so, we won't change anything — it's already expanded syntax. Otherwise, we assume that it is the short-cut syntax, and expand it.

Second, we can't directly access the value any longer. We need to instead use the `$eq` key to do that at the end of line 11.

## `array-filter.test.5.js`

We'd like to start supporting other operators, such as `$lt` for "less than". Does this test case make sense to you?

## `array-filter.5.js`

Now, how do we want to change the code?

Let's make an object where the keys are the operator names and the values are some comparison functions! Each comparison function takes two parameters — `a` and `b` — that will compare according to the appropriate rule and return a boolean as to whether the row ought to be included or not.

Now, we need to make use of the appropriate operator — we can't hard-code the `$eq` operator any longer!

Step one is to take the rule — `$lt: "b"` — into the operator `$lt` and the value `b`. We can do this with `Object.entries`. (This returns an array, of which each element is a two-element array of the form `[key, value]`.)

Step two: replace the direct comparison `row[field] === rule.$eq` with a call to the appropriate comparison operator function `comparison_operators[operator](row[field], value)`.

## `array-filter.test.6.js`

Now we can add in a bunch of other comparison operators — not equal to `$neq`, less than or equal to `$lte`, greater than `$gt`, and greater than or equal to `$gte`. I won't dig into the tests…

## `array-filter.6.js`

…and you can see the implementations. Fairly straightforward.

## `array-filter.test.7.js`

Now, let's look at combining some of these operators. Say you are looking for an `id` that is simultaneously greater than 1 and less than 3. A query to express that would be:

```json
{
  id: {
    $gt: 1,
    $lt: 3
  }
}
```

## `array-filter.7.js`

How can we make our query engine support that?

We are only grabbing the first rule for each field — `Object.entries(rule)[0]`. But `Object.entries` is returning an array — what if we loop through all the elements (rules) for that field, and combine those — in this case, with boolean `and` just like we have been.

## `array-filter.8.js`

Okay, now I'm going to pull out the anonymous `filter` function into a function of its own: `doesRowFitQuery`. No change in the logic. (I have the advantage of hindsight…)

## `array-filter.test.9.js`

Let's support boolean combination operators `$and` and `$or`.

All the clauses inside the array are combined with boolean `and` — they all need to be satisfied. Make sense?

## `array-filter.9.js`

Let's take a page out of the trick we did for the other operators and make a JavaScript object that has the operator — `$and` in this case — as the key and a function as the value. This function will take in a row — remember all we need to do is examine each row individually to see if it satisfies the query — and an `$and`-type query.

Really, for the array of criteria that the `$and` operator has, we just want to loop through them all, evaluate them, and return whether or not they all return `true`. Let's use `Array.reduce`.

Then, in `doesRowFitQuery` itself, we need to check and see if the query we are looking at is a "combination" query — if so, we need to run the combination operator. Otherwise, we'll do the same things we were before.

## `array-filter.test.10.js`

Now, we can add in an `$or` query, as well.

## `array-filter.10.js`

And the implementation is extremely similar to `$and`.

## `array-filter.test.11.js`

Now, we need to make sure that we can nest operators on pseudo-arbitrarily complicated query. And — it works! Nice!

## Questions?

Arguably, there is room for improvements:

1. More tests.
2. More operators.
3. Nested data structures.
4. Better string comparisons (i.e. ignore accents, Unicode normalization, "natural" numbers)
5. Performance! (a proper index, web workers, parallelization, short circuits, inline operators for function calls, etc.)

Any questions?