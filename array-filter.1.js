export default function filter({ array, query = {} }) {
  if (Object.keys(query).length === 0) {
    return [];
  }
  const value = Object.values(query)[0];
  return array.filter(function (row) {
    return row.id === value;
  });
}
