const comparison_operators = {
  $eq: function (a, b) {
    return a === b;
  },
  $lt: function (a, b) {
    return a < b;
  },
  $neq: function (a, b) {
    return a !== b;
  },
  $lte: function (a, b) {
    return a <= b;
  },
  $gt: function (a, b) {
    return a > b;
  },
  $gte: function (a, b) {
    return a >= b;
  },
};

export default function filter({ array, query = {} }) {
  if (Object.keys(query).length === 0) {
    return [];
  }
  return array.filter(function (row) {
    let include_row = true;
    for (let [field, rule] of Object.entries(query)) {
      if (typeof rule !== "object") {
        rule = { $eq: rule };
      }
      for (const [operator, value] of Object.entries(rule)) {
        include_row =
          include_row && comparison_operators[operator](row[field], value);
      }
    }
    return include_row;
  });
}
